<?php
// This script is included in the showXYZ.php scripts.
// In those scripts $document has been initialized with the current post

echo '<h2>Comentarios</h2>';

// Show the comments of the current post
echo '<ul>';

foreach($document['comments'] as $comment)
{
  $textcomment = substr($comment['body'],0,100);
  $cauthor=$comment['author'];
  echo "<li>(<a href='index.php?command=showPostsCommentedByAuthor&author=$cauthor'>$cauthor</a>)$textcomment</li>";
}

echo '</ul>';
?>
