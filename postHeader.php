<?php
// This script is included in the showXYZ.php scripts.
// In those scripts $document has been initialized with the current post

	echo '<div class="blog-post">';

	// Show post title
	echo '<h2 class="blog-post-title">';
	$title = $document['title'];
	echo $title;
	echo '</h2>';

	// Show post date and author
	echo '<p class="blog-post-meta">';
	$date = $document['date'];
	echo $date->toDateTime()->format('l, d F Y');
	echo ' by <a href="#">';

	$email = $document['email'];
	$author = $document['author'];
	echo $author;
	echo '</a></p>';
?>