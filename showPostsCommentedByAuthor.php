<?php
// Get the author to be found
$author = $_GET['author'];

// Connect to the database 
include_once("connection.php");

// Get 5 posts commented by the author
$documents = $collection->find(array('comments.author' => $author), array('limit' => 5));

foreach($documents as $document) {
	// Include the header of the post (title and date)	
	include("postHeader.php");

	//Show the first 300 characters of the post body
	$text = substr($document['body'],0,300) . '...';
	echo $text;

	// Setup an hyperlink to obtain the full text of the post
	// The hyperlink points to index.php with this two GET parameters:
	// command: 'showMore' 
	// id: the string of the _id of the post
	$id = $document['_id'];
	echo '<a href = index.php?command=showMore&id=' . $id . '> Mostrar más</a>';
	
	// Include the labels of the post
	include("labels.php");

	// Include the comments of the post
	include("comments.php");

	echo '</div>';
}
?>
